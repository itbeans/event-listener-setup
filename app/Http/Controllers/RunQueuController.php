<?php

namespace App\Http\Controllers;

use App\Events\DemoEvent;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Artisan;

class RunQueuController extends Controller
{
    public function index(){
        dump("Running Queue Worker...");
        Artisan::call('queue:work',['--tries'=>3,'--once']);
    }
    public function testEvent(){
        event(new DemoEvent());
    }
}
