<?php

namespace App\Providers;
use App\Events\DemoEvent;
use App\Events\DemoSubEvent;
use App\Listeners\DemoListener;
use App\Listeners\DemoSubListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        DemoEvent::class=>[
            DemoListener::class,
        ],
        DemoSubEvent::class=>[
            DemoSubListener::class,
        ]


    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
